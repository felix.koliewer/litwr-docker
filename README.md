# litwr-docker

## Startup

```
cd /home
git clone git@gitlab.com:felix.koliewer/litwr-docker.git minecraft-litw/
cd minecraft-litw
docker volume create minecraft_litw_data
docker-compose up -d --force-recreate
```

## Shutdown

```
docker-compose down
```

or use something like portainer, swarmpit ...

